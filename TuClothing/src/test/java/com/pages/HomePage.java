package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;

public class HomePage extends BaseClass {
	
	public static WebDriverWait wait;
	public static By SEARCHTEXTBOX = By.cssSelector("#search");
	public static By SEARCHBUTTON = By.cssSelector(".searchButton");
	public static By ACCEPTCOOKIE = By.cssSelector("#consent_prompt_submit");
	public static By STORELOCATORLINK = By.linkText("Tu Store Locator");
	public static By PLEASECOMPLETESEARCH = By.cssSelector(".form_field_error-message");
	
	public void verifyHomePage() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\bgh07818\\Documents\\Automation\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize(); 
		driver.get("https://tuclothing.sainsburys.co.uk/");	
	}
	public void searchWithValidProductName() {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(ACCEPTCOOKIE)).click();
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("Jeans");
		driver.findElement(SEARCHBUTTON).click();	
	}
	public void searchWithInvalidProductName() {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(ACCEPTCOOKIE)).click();
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("alcohol");
		driver.findElement(SEARCHBUTTON).click();		
	}
	public void searchWithEmptyBox() {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(ACCEPTCOOKIE)).click();
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("");
		driver.findElement(SEARCHBUTTON).click();
	}
	
	public void searchWithValidProductCategoryName() {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(ACCEPTCOOKIE)).click();
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("School Uniform");
		driver.findElement(SEARCHBUTTON).click(); 
	}
	
	public void searchWithColour() {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(ACCEPTCOOKIE)).click();
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("Yellow");
		driver.findElement(SEARCHBUTTON).click();
	}
	
	public void searchWithSearchIcon() {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(ACCEPTCOOKIE)).click();
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHBUTTON).click();
	}
	
	public void verifySearchIcon() {
		Assert.assertEquals("Please complete a product search", driver.findElement(PLEASECOMPLETESEARCH).getText());
	}
	
	public void searchTextBox() {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(ACCEPTCOOKIE)).click();
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("");
		driver.findElement(SEARCHBUTTON).click();
	}
	
	public void verifySearchTextBox() {
		Assert.assertEquals("Please complete a product search", driver.findElement(PLEASECOMPLETESEARCH).getText());
	}
	
	public void searchForValidProductNumber() {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(ACCEPTCOOKIE)).click();
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("137144822");
		wait.until(ExpectedConditions.elementToBeClickable(SEARCHBUTTON)).click();
	}
	
	public void searchForDifferentBrands() {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(ACCEPTCOOKIE)).click();
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("hollister");
		driver.findElement(SEARCHBUTTON).click();
	}
	
	public void searchWithInvalidProductNumber() {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(ACCEPTCOOKIE)).click();
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("843948");
		driver.findElement(SEARCHBUTTON).click();
	}
	
	public void searchWithSpecialCharacters() {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(ACCEPTCOOKIE)).click();
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("$&^*$&");
		driver.findElement(SEARCHBUTTON).click();
	}
	
	
	public void tuStoreLocator() {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(ACCEPTCOOKIE)).click();
		driver.findElement(STORELOCATORLINK).click();
	}
}
