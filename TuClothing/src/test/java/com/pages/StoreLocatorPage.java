package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class StoreLocatorPage extends HomePage {
	public static By POSTCODETXTBOX = By.name("q");
	public static By STORELOCATORBUTTON = By.cssSelector("button[class='ln-c-button ln-c-button--primary'");
	public static By STORELOCATORTXT = By.cssSelector("h1");
	public static By WOMENCHECKBOX = By.cssSelector("label[for='women'");
	public static By MENCHECKBOX = By.cssSelector("label[for='men'");
	public static By CHILDRENCHECKBOX = By.cssSelector("label[for='children'");
	public static By CLICKCOLLECTCHKBOX = By.cssSelector("label[for='click'");
	
	
	public void verifyTuStoreLocator() {
		Assert.assertEquals("Store Locator", driver.findElement(STORELOCATORTXT).getText());
	}

	public void storeLocatorWithValidPostcode() {
		wait.until(ExpectedConditions.elementToBeClickable(POSTCODETXTBOX));
		driver.findElement(POSTCODETXTBOX).clear();
		driver.findElement(POSTCODETXTBOX).sendKeys("RG142FB");
		driver.findElement(STORELOCATORBUTTON).click();
	}
	
	public void storeLocatorWithInvalidPostcode() {
		wait.until(ExpectedConditions.elementToBeClickable(POSTCODETXTBOX));
		driver.findElement(POSTCODETXTBOX).clear();
		driver.findElement(POSTCODETXTBOX).sendKeys("rrrrr");
		driver.findElement(STORELOCATORBUTTON).click();
	}
	
	public void storeLocatorWithValidTown() {
		wait.until(ExpectedConditions.elementToBeClickable(POSTCODETXTBOX));
		driver.findElement(POSTCODETXTBOX).clear();
		driver.findElement(POSTCODETXTBOX).sendKeys("Newbury");
		driver.findElement(STORELOCATORBUTTON).click();
	}
	
	public void storeLocatorWithInvalidTown() {
		wait.until(ExpectedConditions.elementToBeClickable(POSTCODETXTBOX));
		driver.findElement(POSTCODETXTBOX).clear();
		driver.findElement(POSTCODETXTBOX).sendKeys("qwerty");
		driver.findElement(STORELOCATORBUTTON).click();
	}
	
	public void enterValidPostcode() {
		wait.until(ExpectedConditions.elementToBeClickable(POSTCODETXTBOX));
		driver.findElement(POSTCODETXTBOX).clear();
		driver.findElement(POSTCODETXTBOX).sendKeys("RG142FB");
	}
	
	public void chooseWomenCheckbox() {
		driver.findElement(WOMENCHECKBOX).click();
		driver.findElement(STORELOCATORBUTTON).click();
	}
	
	public void chooseMenCheckbox() {
		driver.findElement(MENCHECKBOX).click();
		driver.findElement(STORELOCATORBUTTON).click();
	}
	
	public void chooseChildrenCheckbox() {
		driver.findElement(CHILDRENCHECKBOX).click();
		driver.findElement(STORELOCATORBUTTON).click();
	}
	
	public void selectClickCollectChkBox() {
		driver.findElement(CLICKCOLLECTCHKBOX).click();
		driver.findElement(STORELOCATORBUTTON).click();
	}
}
