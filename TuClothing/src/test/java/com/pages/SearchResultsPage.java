package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

public class SearchResultsPage extends BaseClass{
	
	public void verifySearchRelatedProducts() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=Jeans", driver.getCurrentUrl());
		
	}
	
	public void verifySearchForInvalidProduct() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=alcohol", driver.getCurrentUrl());
	}
	
	public void verifySearchForEmptyTextBox() {
		Assert.assertEquals("Please complete a product search", driver.findElement(By.cssSelector(".form_field_error-message")).getText());
	}
	
	public void verifySearchForValidProductCategoryName() {
		Assert.assertEquals("School Uniform", driver.findElement(By.cssSelector("a.last")).getText());
	}

	public void verifySearchForColour() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=Yellow", driver.getCurrentUrl());
	}
	
	public void verifySearchWithValidProductNumber() {
		Assert.assertEquals("Search for '137144822'", driver.findElement(By.cssSelector(".ln-c-breadcrumbs__link--last")).getText());
	}
	
	public void verifySearchWithDifferentBrands() {
		Assert.assertEquals("Search for 'hollister'", driver.findElement(By.cssSelector(".ln-c-breadcrumbs__link--last")).getText());
	}
	
	public void verifySearchWithInvalidProductNumber() {
		Assert.assertEquals("Search for '843948'", driver.findElement(By.cssSelector(".ln-c-breadcrumbs__link--last")).getText());
	}
	
	public void verifySearchWithSpecialCharacters() {
		Assert.assertEquals("Search results for: $&^*$&", driver.findElement(By.cssSelector("h1")).getText());
	}
	
}
