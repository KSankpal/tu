package com.runner;

import org.openqa.selenium.WebDriver;
import com.pages.HomePage;
import com.pages.SearchResultsPage;
import com.pages.StoreLocatorPage;

public class BaseClass {
	
	public static WebDriver driver;
	public static HomePage homePage = new HomePage();
	public static SearchResultsPage searchResultsPage = new SearchResultsPage();
	public static StoreLocatorPage storeLocatorPage = new StoreLocatorPage();

}
