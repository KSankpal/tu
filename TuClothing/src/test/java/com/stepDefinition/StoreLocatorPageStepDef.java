package com.stepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.pages.HomePage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StoreLocatorPageStepDef extends HomePage{
	
	@Then("^I should see relevant page$")
	public void i_should_see_relevant_page() throws Throwable {
		storeLocatorPage.verifyTuStoreLocator();
	}
	
	@When("^I Enter valid postcode$")
	public void i_Enter_valid_postcode() throws Throwable {
		storeLocatorPage.storeLocatorWithValidPostcode();
	}

	
	
	@When("^I Enter Invalid postcode$")
	public void i_Enter_Invalid_postcode() throws Throwable {
		storeLocatorPage.storeLocatorWithInvalidPostcode();
	}
	
	
	@When("^I Enter valid town$")
	public void i_Enter_valid_town() throws Throwable {
		storeLocatorPage.storeLocatorWithValidTown();
	}
	
	@When("^I Enter Invalid town$")
	public void i_Enter_Invalid_town() throws Throwable {
		storeLocatorPage.storeLocatorWithInvalidTown();
	}
	
	@When("^I Enter valid town/postcode$")
	public void i_Enter_valid_town_postcode() throws Throwable {
		storeLocatorPage.enterValidPostcode();
	}
	
	@When("^select women checkbox$")
	public void select_women_checkbox() throws Throwable {
		storeLocatorPage.chooseWomenCheckbox();
	}
	
	@When("^select men checkbox$")
	public void select_men_checkbox() throws Throwable {
		storeLocatorPage.chooseMenCheckbox();
	}
	
	@When("^select children checkbox$")
	public void select_children_checkbox() throws Throwable {
		storeLocatorPage.chooseChildrenCheckbox();
	}
	
	@When("^select click and collect check box$")
	public void select_click_and_collect_check_box() throws Throwable {
		
	}
}
