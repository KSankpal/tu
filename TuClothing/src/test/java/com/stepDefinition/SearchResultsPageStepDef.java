package com.stepDefinition;
import com.runner.BaseClass;
import cucumber.api.java.en.Then;

public class SearchResultsPageStepDef extends BaseClass {
	
	@Then("^I should see the Search related products$")
	public void i_should_see_the_Search_related_products() throws Throwable {
		
		searchResultsPage.verifySearchRelatedProducts();
	}
	
	@Then("^I should see dissimilar products$")
	public void i_should_see_dissimilar_products() throws Throwable {
		
		searchResultsPage.verifySearchForInvalidProduct();
	}
	
	@Then("^I should see an error massage$")
	public void i_should_see_an_error_massage() throws Throwable {
		searchResultsPage.verifySearchForEmptyTextBox();
	}
	
	@Then("^I should see the search related products$")
	public void i_should_see_the_search_related_products() throws Throwable {
		searchResultsPage.verifySearchForValidProductCategoryName();
	}
	
	@Then("^I should see the colour related products$")
	public void i_should_see_the_colour_related_products() throws Throwable {
		searchResultsPage.verifySearchForColour();
	}
	
	@Then("^I should see item with matching product number$")
	public void i_should_see_item_with_matching_product_number() throws Throwable {
		searchResultsPage.verifySearchWithValidProductNumber();
	}
	
	@Then("^I should the search related products$")
	public void i_should_the_search_related_products() throws Throwable {
		searchResultsPage.verifySearchWithDifferentBrands();
	}
	
	@Then("^I should see an error massage Sorry no results found$")
	public void i_should_see_an_error_massage_Sorry_no_results_found() throws Throwable {
		searchResultsPage.verifySearchWithInvalidProductNumber();
	}
	
	@Then("^I should see an error message$")
	public void i_should_see_an_error_message() throws Throwable {
		searchResultsPage.verifySearchWithSpecialCharacters();
	}

}
