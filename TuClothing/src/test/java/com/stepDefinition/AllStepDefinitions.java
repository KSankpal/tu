package com.stepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AllStepDefinitions {
	public static WebDriver driver;
	public static  WebDriverWait wait;
	
	

	
	

	
	
	

	
	

	
	
	

	
	
	

	
	

	
	
	

	
	
	

	

	

	

	
	

	
	
	
	

	
	
	

	
	
	

	

	
	
	

	

	

	
	
	

	

	

	
	
	

	@Then("^I should see List of nearest store and stores that have click and collect option$")
	public void i_should_see_List_of_nearest_store_and_stores_that_have_click_and_collect_option() throws Throwable {
		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
	}
	
	@When("^I Click on Find store$")
	public void i_Click_on_Find_store() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.name("q")));
		driver.findElement(By.name("q")).clear();
		driver.findElement(By.name("q")).sendKeys("");
		driver.findElement(By.cssSelector("button[class='ln-c-button ln-c-button--primary'")).click();
	}

	@Then("^I should see an error massage to enter text field$")
	public void i_should_see_an_error_massage_to_enter_text_field() throws Throwable {
		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
	}
	
	@When("^Select click & collect check box$")
	public void select_click_collect_check_box() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.name("q")));
		driver.findElement(By.name("q")).clear();
		driver.findElement(By.name("q")).sendKeys("");
		driver.findElement(By.cssSelector("label[for='click'")).click();
	}

	@When("^Click on Find store$")
	public void click_on_Find_store() throws Throwable {
		driver.findElement(By.cssSelector("button[class='ln-c-button ln-c-button--primary'")).click();
	}

	@Then("^I should see an error massage to enter valid postcode$")
	public void i_should_see_an_error_massage_to_enter_valid_postcode() throws Throwable {
		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
	}
	
	@When("^Enter valid town/postcode$")
	public void enter_valid_town_postcode() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.name("q")));
		driver.findElement(By.name("q")).clear();
		driver.findElement(By.name("q")).sendKeys("Newbury");
	}

	@When("^Select Womens, Mens, Childrens and click & collect check box$")
	public void select_Womens_Mens_Childrens_and_click_collect_check_box() throws Throwable {
		driver.findElement(By.cssSelector("label[for='women'")).click();
		driver.findElement(By.cssSelector("label[for='men'")).click();
		driver.findElement(By.cssSelector("label[for='children'")).click();
		driver.findElement(By.cssSelector("label[for='click'")).click();
	}

	@Then("^I should see List of nearest stores, stores with clothing stock and stores that have click & collect option$")
	public void i_should_see_List_of_nearest_stores_stores_with_clothing_stock_and_stores_that_have_click_collect_option() throws Throwable {
		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
	}
	
	@When("^Enter Invalid town/postcode$")
	public void enter_Invalid_town_postcode() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.name("q")));
		driver.findElement(By.name("q")).clear();
		driver.findElement(By.name("q")).sendKeys("rrrrr");
	}
	
	@Then("^I should see an error message to enter valid postcode$")
	public void i_should_see_an_error_message_to_enter_valid_postcode() throws Throwable {
		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
	}
	
	@When("^I Click on Login/Register$")
	public void i_Click_on_Login_Register() throws Throwable {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.linkText("Tu Log In / Register")).click();
	}

	@Then("^I should see Login/Register page$")
	public void i_should_see_Login_Register_page() throws Throwable {
		Assert.assertEquals("Log in with your Tu details", driver.findElement(By.cssSelector(".loginFormHolder h3")).getText());
	}
	
	@When("^Click on Register icon$")
	public void click_on_Register_icon() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button[data-testid='registerButton']")));
		driver.findElement(By.cssSelector("button[data-testid='registerButton']")).click();
	}

	@Then("^I should see registration page$")
	public void i_should_see_registration_page() throws Throwable {
		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".last h2")).getText());
	}

	@When("^Enter the valid relevant information$")
	public void enter_the_valid_relevant_information() throws Throwable {
		driver.findElement(By.id("register_email")).clear();
		driver.findElement(By.id("register_email")).sendKeys("krutika@gmail.com");
		Select titleDropdown = new Select(driver.findElement(By.id("register_title")));
		titleDropdown.selectByValue("mrs");
		driver.findElement(By.id("register_firstName")).clear();
		driver.findElement(By.id("register_firstName")).sendKeys("krutika");
		driver.findElement(By.id("register_lastName")).sendKeys("sankpal");
		driver.findElement(By.id("password")).sendKeys("nainu_1");
		driver.findElement(By.id("register_checkPwd")).sendKeys("nainu");
	}

	@When("^click on COMPLETE REGISTRATION$")
	public void click_on_COMPLETE_REGISTRATION() throws Throwable {
		driver.findElement(By.cssSelector("button[data-testid='completeRegistration'")).click();
	}

	@Then("^I should see Thank you for registering message$")
	public void i_should_see_Thank_you_for_registering_message() throws Throwable {
		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".last h2")).getText());
	}
	@When("^Enter the Invalid email$")
	public void enter_the_Invalid_email() throws Throwable {
		driver.findElement(By.id("register_email")).clear();
		driver.findElement(By.id("register_email")).sendKeys("abcs");
	}

	@Then("^I should see an error message to enter valid email$")
	public void i_should_see_an_error_message_to_enter_valid_email() throws Throwable {
		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".last h2")).getText());
	}
	
	@When("^Enter special character$")
	public void enter_special_character() throws Throwable {
		driver.findElement(By.id("register_firstName")).clear();
		driver.findElement(By.id("register_firstName")).sendKeys("$$$$$");
	}

	@Then("^I should see error message$")
	public void i_should_see_error_message() throws Throwable {
		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".last h2")).getText());
	}
	
	@When("^I Click on Register icon$")
	public void i_Click_on_Register_icon() throws Throwable {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.linkText("Tu Log In / Register")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button[data-testid='registerButton']")));
		driver.findElement(By.cssSelector("button[data-testid='registerButton']")).click();
	}

	@Then("^I should see an error message to fill text box$")
	public void i_should_see_an_error_message_to_fill_text_box() throws Throwable {
		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".last h2")).getText());
	}

	@When("^Enter less than (\\d+)-character password$")
	public void enter_less_than_character_password(int arg1) throws Throwable {
		driver.findElement(By.id("password")).sendKeys("na");
		driver.findElement(By.id("register_checkPwd")).sendKeys("na");
	}

	@Then("^I should see error message to re-enter password$")
	public void i_should_see_error_message_to_re_enter_password() throws Throwable {
		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".last h2")).getText());
	}
	
	@When("^Enter Invalid nectar card number$")
	public void enter_Invalid_nectar_card_number() throws Throwable {
		driver.findElement(By.cssSelector("#regNectarPointsOne")).sendKeys("12345");
		driver.findElement(By.cssSelector("#regNectarPointsTwo")).sendKeys("12345");

	}

	@Then("^I should see an error message as Invalid Nectar card number$")
	public void i_should_see_an_error_message_as_Invalid_Nectar_card_number() throws Throwable {
		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".last h2")).getText());
	}
	
	@When("^click on COMPLETE REGISTRATION without ticking \"([^\"]*)\" check box$")
	public void click_on_COMPLETE_REGISTRATION_without_ticking_check_box(String arg1) throws Throwable {
		driver.findElement(By.id("register_email")).clear();
		driver.findElement(By.id("register_email")).sendKeys("krutika@gmail.com");
		Select titleDropdown = new Select(driver.findElement(By.id("register_title")));
		titleDropdown.selectByValue("mrs");
		driver.findElement(By.id("register_firstName")).clear();
		driver.findElement(By.id("register_firstName")).sendKeys("krutika");
		driver.findElement(By.id("register_lastName")).sendKeys("sankpal");
		driver.findElement(By.id("password")).sendKeys("asdfghh");
		driver.findElement(By.id("register_checkPwd")).sendKeys("asdfghh");
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#submit-register")));
		driver.findElement(By.cssSelector("#submit-register")).click();
	}

	@Then("^I should see an error message to click on I accept$")
	public void i_should_see_an_error_message_to_click_on_I_accept() throws Throwable {
		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".last h2")).getText());
	}

	@When("^click on COMPLETE REGISTRATION without choosing title$")
	public void click_on_COMPLETE_REGISTRATION_without_choosing_title() throws Throwable {
		
		driver.findElement(By.id("register_email")).clear();
		driver.findElement(By.id("register_email")).sendKeys("krutika@gmail.com");
		Select titleDropdown = new Select(driver.findElement(By.id("register_title")));
		titleDropdown.selectByValue("");
		driver.findElement(By.id("register_firstName")).clear();
		driver.findElement(By.id("register_firstName")).sendKeys("krutika");
		driver.findElement(By.id("register_lastName")).sendKeys("sankpal");
		driver.findElement(By.id("password")).sendKeys("nainu_1");
		driver.findElement(By.id("register_checkPwd")).sendKeys("nainu");
		driver.findElement(By.cssSelector("button[data-testid='completeRegistration'")).click();
	}

	@Then("^I should see an error message to choose title$")
	public void i_should_see_an_error_message_to_choose_title() throws Throwable {
		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".last h2")).getText());
	}
	
	@When("^Enter valid Email ID and Password$")
	public void enter_valid_Email_ID_and_Password() throws Throwable {
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys("krutika.sankpal@gmail.com");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("Krutika_098");
		driver.findElement(By.cssSelector("#submit-login")).click();
	}

	@Then("^I should see Tu home page$")
	public void i_should_see_Tu_home_page() throws Throwable {
		Assert.assertEquals("Log in with your Tu details", driver.findElement(By.cssSelector(".loginFormHolder h3")).getText());   
	}

	@When("^Enter Invalid Email ID and valid Password$")
	public void enter_Invalid_Email_ID_and_valid_Password() throws Throwable {
		driver.findElement(By.linkText("Tu Log In / Register")).click();
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys("acc");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("Krutika_098");
		driver.findElement(By.cssSelector("#submit-login")).click();
	}

	

	@Then("^I should see error message\\.$")
	public void i_should_see_error_message1() throws Throwable {
		Assert.assertEquals("Log in with your Tu details", driver.findElement(By.cssSelector(".loginFormHolder h3")).getText());
	}

	@When("^Enter valid Email ID and Invalid Password$")
	public void enter_valid_Email_ID_and_Invalid_Password() throws Throwable {
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys("krutika.sankpal@gmail.com");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("asd");
		driver.findElement(By.cssSelector("#submit-login")).click();
	}
	
	@When("^Enter valid Password blank email$")
	public void enter_valid_Password_blank_email() throws Throwable {
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys(".");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("asd");
		driver.findElement(By.cssSelector("#submit-login")).click();
	}
	@When("^Enter valid email and blank Password$")
	public void enter_valid_email_and_blank_Password() throws Throwable {
		driver.findElement(By.cssSelector("#j_username")).clear();
		driver.findElement(By.cssSelector("#j_username")).sendKeys("krutika.sankpal@gmail.com");
		driver.findElement(By.cssSelector("#j_password")).clear();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("");
		driver.findElement(By.cssSelector("#submit-login")).click();
	}
	
	@Given("^I am in product detail page$")
	public void i_am_in_product_detail_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\bgh07818\\Documents\\Automation\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize(); 
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("Jeans");
		driver.findElement(By.className("searchButton")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("img[alt='Mid Denim Slim Jean']")));
		driver.findElement(By.cssSelector("img[alt='Mid Denim Slim Jean']")).click();
	}

	@Given("^I choose the size/qty$")
	public void i_choose_the_size_qty() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("img[alt='Mid Denim Slim Jean']")));
		driver.findElement(By.cssSelector("img[alt='Mid Denim Slim Jean']")).click();
		Select sizeDropdown = new Select(driver.findElement(By.cssSelector("#select-size")));
		sizeDropdown.selectByValue("137503311");
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#productVariantQty.ln-c-select")));
		Select qtyDropdown = new Select(driver.findElement(By.cssSelector("#productVariantQty.ln-c-select")));
		qtyDropdown.selectByValue("2");
		
	}

	@When("^I Click on Add to basket$")
	public void i_Click_on_Add_to_basket() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("button[title='Add a basket']"))));
		driver.findElement(By.cssSelector("button[title='Add a basket']")).click();
	}

	@Then("^I see the product added to basket$")
	public void i_see_the_product_added_to_basket() throws Throwable {
		Assert.assertEquals("Mid Denim Slim Jean", driver.findElement(By.cssSelector(".ln-c-breadcrumbs__link--last")).getText());
	}
	
	@Given("^I am Home Page$")
	public void i_am_Home_Page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\bgh07818\\Documents\\Automation\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize(); 
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
	}

	@When("^I move the cursor on Basket image$")
	public void i_move_the_cursor_on_Basket_image() throws Throwable {
		Actions actions = new Actions(driver);
		actions.moveToElement(driver.findElement(By.cssSelector("#basket-title")));
		actions.perform();
	}

	@Then("^I see a message ?Your basket is empty?$")
	public void i_see_a_message_Your_basket_is_empty() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
	}
	
	@Given("^I am in Basket page with products added$")
	public void i_am_in_Basket_page_with_products_added() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\bgh07818\\Documents\\Automation\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize(); 
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("Jeans");
		driver.findElement(By.className("searchButton")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("img[alt='Mid Denim Slim Jean']")));
		driver.findElement(By.cssSelector("img[alt='Mid Denim Slim Jean']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#select-size")));
		Select sizeDropdown = new Select(driver.findElement(By.cssSelector("#select-size")));
		sizeDropdown.selectByValue("137503311");
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#productVariantQty.ln-c-select")));
		Select qtyDropdown = new Select(driver.findElement(By.cssSelector("#productVariantQty.ln-c-select")));
		qtyDropdown.selectByValue("2");
		driver.findElement(By.cssSelector("button[title='Add a basket']")).click();
		driver.findElement(By.cssSelector("#basket-title")).click();
	}

	@When("^click on \"([^\"]*)\" button$")
	public void click_on_button(String arg1) throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[title='Proceed to Checkout']")));
		driver.findElement(By.cssSelector("div[title='Proceed to Checkout']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".ln-c-button.ln-c-button--secondary.left"))).click();
	}

	@Then("^I see Previous opened page$")
	public void i_see_Previous_opened_page() throws Throwable {
		Assert.assertEquals("Search for 'Jeans'", driver.findElement(By.cssSelector(".ln-c-breadcrumbs__link--last")).getText());
	}
	
	@When("^I click on remove button$")
	public void i_click_on_remove_button() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[title='Proceed to Checkout']")));
		driver.findElement(By.cssSelector("div[title='Proceed to Checkout']")).click();
		driver.findElement(By.cssSelector("#RemoveProduct_0")).click();
	}

	@Then("^I see the product removed$")
	public void i_see_the_product_removed() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart", driver.getCurrentUrl());
	}
	
	@When("^I click on Add a promotion code/ pay with voucher$")
	public void i_click_on_Add_a_promotion_code_pay_with_voucher() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[title='Proceed to Checkout']")));
		driver.findElement(By.cssSelector("div[title='Proceed to Checkout']")).click();
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", driver.findElement(By.cssSelector(".basketTotalsAddPromo.clearfix #showPromo")));
		driver.findElement(By.cssSelector("#voucher_voucherCode")).click();
		
	}

	@Then("^I see an empty text box and apply button$")
	public void i_see_an_empty_text_box_and_apply_button() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart", driver.getCurrentUrl());
	}
	
	@When("^Enter Invalid code$")
	public void enter_Invalid_code() throws Throwable {
		driver.findElement(By.cssSelector("#voucher_voucherCode")).sendKeys("2345");
		driver.findElement(By.cssSelector("button[class='ln-c-button ln-c-button--secondary']")).click();
	}

	@Then("^I see an error message$")
	public void i_see_an_error_message() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart/apply-voucher", driver.getCurrentUrl());
	}
	
	@Given("^products added to basket$")
	public void products_added_to_basket() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("tops");
		driver.findElement(By.className("searchButton")).click();
		Assert.assertEquals("Search for 'tops'", driver.findElement(By.cssSelector(".ln-c-breadcrumbs__link--last")).getText());
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("img[alt='Christmas Bauble Print Top']")));
		driver.findElement(By.cssSelector("img[alt='Christmas Bauble Print Top']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[data-value='138001330']")));
		driver.findElement(By.cssSelector("div[data-value='138001330']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#productVariantQty.ln-c-select")));
		Select qtyDropdown = new Select(driver.findElement(By.cssSelector("#productVariantQty.ln-c-select")));
		qtyDropdown.selectByValue("2");
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#AddToCart")));
		driver.findElement(By.cssSelector("#AddToCart")).click();
		Assert.assertEquals("Womens Christmas Bauble Print Top | Tu clothing", driver.getTitle());
		driver.findElement(By.cssSelector("#basket-title")).click();
	}

	@When("^I Click on Checkout$")
	public void i_Click_on_Checkout() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[title='Proceed to Checkout']")));
		driver.findElement(By.cssSelector("div[title='Proceed to Checkout']")).click();
	}

	@Then("^I see Basket page$")
	public void i_see_Basket_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart", driver.getCurrentUrl());

	}
	@Given("^I am in Basket page$")
	public void i_am_in_Basket_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\bgh07818\\Documents\\Automation\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize(); 
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("tops");
		driver.findElement(By.className("searchButton")).click();
		Assert.assertEquals("Search for 'tops'", driver.findElement(By.cssSelector(".ln-c-breadcrumbs__link--last")).getText());
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("img[alt='Christmas Bauble Print Top']")));
		driver.findElement(By.cssSelector("img[alt='Christmas Bauble Print Top']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[data-value='138001330']")));
		driver.findElement(By.cssSelector("div[data-value='138001330']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#productVariantQty.ln-c-select")));
		Select qtyDropdown = new Select(driver.findElement(By.cssSelector("#productVariantQty.ln-c-select")));
		qtyDropdown.selectByValue("2");
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#AddToCart")));
		driver.findElement(By.cssSelector("#AddToCart")).click();
		Assert.assertEquals("Womens Christmas Bauble Print Top | Tu clothing", driver.getTitle());
		driver.findElement(By.cssSelector("#basket-title")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[title='Proceed to Checkout']")));
		driver.findElement(By.cssSelector("div[title='Proceed to Checkout']")).click();
	}

	@When("^I Click on Proceed to checkout$")
	public void i_Click_on_Proceed_to_checkout() throws Throwable {
		driver.findElement(By.cssSelector(".ln-c-button.ln-c-button--primary.tuButton")).click();
	}

	@Then("^I see relevant page$")
	public void i_see_relevant_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login/checkout", driver.getCurrentUrl());
	}

	@Given("^I am in Basket page with products added in the basket$")
	public void i_am_in_Basket_page_with_products_added_in_the_basket() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\bgh07818\\Documents\\Automation\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize(); 
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("tops");
		driver.findElement(By.className("searchButton")).click();
		Assert.assertEquals("Search for 'tops'", driver.findElement(By.cssSelector(".ln-c-breadcrumbs__link--last")).getText());
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("img[alt='Christmas Bauble Print Top']")));
		driver.findElement(By.cssSelector("img[alt='Christmas Bauble Print Top']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[data-value='138001330']")));
		driver.findElement(By.cssSelector("div[data-value='138001330']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#productVariantQty.ln-c-select")));
		Select qtyDropdown = new Select(driver.findElement(By.cssSelector("#productVariantQty.ln-c-select")));
		qtyDropdown.selectByValue("1");
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#AddToCart")));
		driver.findElement(By.cssSelector("#AddToCart")).click();
		Assert.assertEquals("Womens Christmas Bauble Print Top | Tu clothing", driver.getTitle());
		driver.findElement(By.cssSelector("#basket-title")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[title='Proceed to Checkout']")));
		driver.findElement(By.cssSelector("div[title='Proceed to Checkout']")).click();
		driver.findElement(By.cssSelector(".ln-c-button.ln-c-button--primary.tuButton")).click();
	}

	@When("^I do a guest checkout$")
	public void i_do_a_guest_checkout() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#guest_email")));
		driver.findElement(By.cssSelector("#guest_email")).clear();
		driver.findElement(By.cssSelector("#guest_email")).sendKeys("anvika@gmail.com");
		driver.findElement(By.cssSelector("button[data-testid='guest_checkout']")).click();
	}

	@Then("^I see click & collect option is unavailable$")
	public void i_see_click_collect_option_is_unavailable() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/delivery-options/choose", driver.getCurrentUrl());
	}

	@Given("^I am in Basket page with basket above twenty pounds$")
	public void i_am_in_Basket_page_with_basket_above_twenty_pounds() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\bgh07818\\Documents\\Automation\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize(); 
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("tops");
		driver.findElement(By.className("searchButton")).click();
		Assert.assertEquals("Search for 'tops'", driver.findElement(By.cssSelector(".ln-c-breadcrumbs__link--last")).getText());
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("img[alt='Christmas Bauble Print Top']")));
		driver.findElement(By.cssSelector("img[alt='Christmas Bauble Print Top']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[data-value='138001330']")));
		driver.findElement(By.cssSelector("div[data-value='138001330']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#productVariantQty.ln-c-select")));
		Select qtyDropdown = new Select(driver.findElement(By.cssSelector("#productVariantQty.ln-c-select")));
		qtyDropdown.selectByValue("3");
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#AddToCart")));
		driver.findElement(By.cssSelector("#AddToCart")).click();
		Assert.assertEquals("Womens Christmas Bauble Print Top | Tu clothing", driver.getTitle());
		driver.findElement(By.cssSelector("#basket-title")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[title='Proceed to Checkout']")));
		driver.findElement(By.cssSelector("div[title='Proceed to Checkout']")).click();
		driver.findElement(By.cssSelector(".ln-c-button.ln-c-button--primary.tuButton")).click();
	}

	@Then("^I see click & collect option available$")
	public void i_see_click_collect_option_available() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/delivery-options/choose", driver.getCurrentUrl());
	}
	
	@Given("^I am in Basket page with products added to basket$")
	public void i_am_in_Basket_page_with_products_added_to_basket() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\bgh07818\\Documents\\Automation\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize(); 
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("tops");
		driver.findElement(By.className("searchButton")).click();
		Assert.assertEquals("Search for 'tops'", driver.findElement(By.cssSelector(".ln-c-breadcrumbs__link--last")).getText());
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("img[alt='Christmas Bauble Print Top']")));
		driver.findElement(By.cssSelector("img[alt='Christmas Bauble Print Top']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[data-value='138001330']")));
		driver.findElement(By.cssSelector("div[data-value='138001330']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#productVariantQty.ln-c-select")));
		Select qtyDropdown = new Select(driver.findElement(By.cssSelector("#productVariantQty.ln-c-select")));
		qtyDropdown.selectByValue("3");
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#AddToCart")));
		driver.findElement(By.cssSelector("#AddToCart")).click();
		Assert.assertEquals("Womens Christmas Bauble Print Top | Tu clothing", driver.getTitle());
		driver.findElement(By.cssSelector("#basket-title")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[title='Proceed to Checkout']")));
		driver.findElement(By.cssSelector("div[title='Proceed to Checkout']")).click();
		driver.findElement(By.cssSelector(".ln-c-button.ln-c-button--primary.tuButton")).click();
	}

	@Then("^I can opt Home Delivery$")
	public void i_can_opt_Home_Delivery() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/delivery-options/choose", driver.getCurrentUrl());
	}
	
	@When("^I enter valid Email in Sign up to get marketing and offers from Tu$")
	public void i_enter_valid_Email_in_Sign_up_to_get_marketing_and_offers_from_Tu() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.cssSelector("#email-sign-up-input")).sendKeys("krutika.sankpal@gmail.com");
		driver.findElement(By.cssSelector(".ln-c-icon")).click();
	}

	@Then("^I see a message \"([^\"]*)\"$")
	public void i_see_a_message(String arg1) throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
	}
	
	@When("^I enter Invalid Email in Sign up to get marketing and offers from Tu$")
	public void i_enter_Invalid_Email_in_Sign_up_to_get_marketing_and_offers_from_Tu() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.cssSelector("#email-sign-up-input")).sendKeys("abcdefg");
		driver.findElement(By.cssSelector(".ln-c-icon")).click();
	}
	
	@When("^I enter Blank Email in Sign up to get marketing and offers from Tu$")
	public void i_enter_Blank_Email_in_Sign_up_to_get_marketing_and_offers_from_Tu() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.cssSelector("#email-sign-up-input")).sendKeys("");
		driver.findElement(By.cssSelector(".ln-c-icon")).click();
	}
	
	@When("^I Click on Facebook logo located on the footer$")
	public void i_Click_on_Facebook_logo_located_on_the_footer() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElements(By.cssSelector(".ln-u-soft-ends")).get(2).click();
	}

	@Then("^I see Tu Facebook homepage page$")
	public void i_see_Tu_Facebook_homepage_page() throws Throwable {
		Assert.assertEquals("https://www.facebook.com/tuclothing", driver.getCurrentUrl());
	}
	
	@When("^I Click on Twitter logo located on the footer$")
	public void i_Click_on_Twitter_logo_located_on_the_footer() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElements(By.cssSelector(".ln-u-soft-ends")).get(3).click();
	}

	@Then("^I see Tu Twitter homepage page$")
	public void i_see_Tu_Twitter_homepage_page() throws Throwable {
		Assert.assertEquals("https://twitter.com/tu_clothing", driver.getCurrentUrl());
	}
	
	@When("^I Click on Instagram logo located on the footer$")
	public void i_Click_on_Instagram_logo_located_on_the_footer() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElements(By.cssSelector(".ln-u-soft-ends")).get(4).click();
	}

	@Then("^I see Tu Instagram homepage page$")
	public void i_see_Tu_Instagram_homepage_page() throws Throwable {
		Assert.assertEquals("https://www.instagram.com/accounts/login/?next=/tuclothing/", driver.getCurrentUrl());
	}
	
	@When("^I Click on Google Plus logo located on the footer$")
	public void i_Click_on_Google_Plus_logo_located_on_the_footer() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElements(By.cssSelector(".ln-u-soft-ends")).get(5).click();	}

	@Then("^I see Tu Google Plus homepage page$")
	public void i_see_Tu_Google_Plus_homepage_page() throws Throwable {
		Assert.assertEquals("Sign in � Google accounts", driver.getTitle());
	}
	
	@When("^I Click on Pinterest logo located on the footer$")
	public void i_Click_on_Pinterest_logo_located_on_the_footer() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElements(By.cssSelector(".ln-u-soft-ends")).get(6).click();
	}

	@Then("^I see Tu Pinterest homepage page$")
	public void i_see_Tu_Pinterest_homepage_page() throws Throwable {
		Assert.assertEquals("https://www.pinterest.com/tuclothing/", driver.getCurrentUrl());
	}
	
	@Given("^I am in Product$")
	public void i_am_in_Product() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\bgh07818\\Documents\\Automation\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize(); 
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("Yellow");
		driver.findElement(By.className("searchButton")).click();
	}

	@When("^I choose High to Low in Sort by$")
	public void i_choose_High_to_Low_in_Sort_by() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#sortOptions1")));
		Select titleDropdown = new Select(driver.findElement(By.cssSelector("#sortOptions1")));
		titleDropdown.selectByValue("relevance");
	}

	@Then("^I see Products sorted$")
	public void i_see_Products_sorted() throws Throwable {
		Assert.assertEquals("Search for 'Yellow'", driver.findElement(By.cssSelector("a.ln-c-breadcrumbs__link--last")).getText());
	}

	@When("^I choose Low to Highin Sort by$")
	public void i_choose_Low_to_Highin_Sort_by() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#sortOptions1")));
		Select titleDropdown = new Select(driver.findElement(By.cssSelector("#sortOptions1")));
		titleDropdown.selectByValue("price-asc");
	}
	
	@When("^I choose Relevance in Sort by$")
	public void i_choose_Relevance_in_Sort_by() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#sortOptions1")));
		Select titleDropdown = new Select(driver.findElement(By.cssSelector("#sortOptions1")));
		titleDropdown.selectByValue("relevance");
	}

	@Given("^I am in Product listing page$")
	public void i_am_in_Product_listing_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\bgh07818\\Documents\\Automation\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize(); 
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("Pink");
		driver.findElement(By.className("searchButton")).click();
		
	}

	@When("^I Choose any facet from \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\"$")
	public void i_Choose_any_facet_from(String arg1, String arg2, String arg3, String arg4, String arg5) throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("label[for='Girls-d']")));
		driver.findElement(By.cssSelector("label[for='Girls-d']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("label[for='Cardigans-d']")));
		driver.findElement(By.cssSelector("label[for='Cardigans-d']")).click(); //Type
		JavascriptExecutor jse=(JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,800)");
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("label[for='4 years_228-d']")));
		driver.findElement(By.cssSelector("label[for='4 years_228-d']")).click(); //Size
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("label[for='Red-d']")));
		driver.findElement(By.cssSelector("label[for='Red-d']")).click(); //colour
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("label[for='�0-�4.99-d']")));
		driver.findElement(By.cssSelector("label[for='�0-�4.99-d']")).click(); //Price
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("label[for='�5-�9.99-d']")));
		driver.findElement(By.cssSelector("label[for='�5-�9.99-d']")).click(); //Price
	}

	@Then("^I see the products sorted based on chosen facet$")
	public void i_see_the_products_sorted_based_on_chosen_facet() throws Throwable {
		Assert.assertEquals("Tu clothing", driver.getTitle());
	}

	
	
	@Given("^I am in Checkout Page$")
	public void i_am_in_Checkout_Page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\bgh07818\\Documents\\Automation\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize(); 
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#consent_prompt_submit"))).click();
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("tops");
		driver.findElement(By.className("searchButton")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("img[alt='Christmas Bauble Print Top']")));
		driver.findElement(By.cssSelector("img[alt='Christmas Bauble Print Top']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[data-value='138001330']")));
		driver.findElement(By.cssSelector("div[data-value='138001330']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#productVariantQty.ln-c-select")));
		Select qtyDropdown = new Select(driver.findElement(By.cssSelector("#productVariantQty.ln-c-select")));
		qtyDropdown.selectByValue("3");
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#AddToCart")));
		driver.findElement(By.cssSelector("#AddToCart")).click();
		driver.findElement(By.cssSelector("#basket-title")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[title='Proceed to Checkout']")));
		driver.findElement(By.cssSelector("div[title='Proceed to Checkout']")).click();
		driver.findElement(By.cssSelector(".ln-c-button.ln-c-button--primary.tuButton")).click();
	}

	@When("^I Enter email in checkout as guest and click on \"([^\"]*)\" button$")
	public void i_Enter_email_in_checkout_as_guest_and_click_on_button(String arg1) throws Throwable {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#guest_email")));
		driver.findElement(By.cssSelector("#guest_email")).clear();
		driver.findElement(By.cssSelector("#guest_email")).sendKeys("anvika@gmail.com");
		driver.findElement(By.cssSelector("button[data-testid='guest_checkout']")).click();
	}

	@When("^I choose \"([^\"]*)\" Delivery Option,find nearest store and click on \"([^\"]*)\"$")
	public void i_choose_Delivery_Option_find_nearest_store_and_click_on(String arg1, String arg2) throws Throwable {
		wait = new WebDriverWait(driver,10);
		driver.findElements(By.cssSelector(".ln-c-form-option__label")).get(0).click();
		driver.findElement(By.cssSelector(".ln-c-button.ln-c-button--primary")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[class='LocationLookup-module__input--2cVMS null']")));
		driver.findElement(By.cssSelector("input[class='LocationLookup-module__input--2cVMS null']")).sendKeys("RG142BH");
		driver.findElement(By.cssSelector(".Button-module__button--3WHb5")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".StoreCard-module__selectStoreButton--3CFno")));
		driver.findElements(By.cssSelector(".StoreCard-module__selectStoreButton--3CFno")).get(0).click();
		driver.findElement(By.cssSelector(".proceed-to-summary.ln-c-button.ln-c-button--primary")).click();
		
	}

	@Then("^I review the order, \"([^\"]*)\"$")
	public void i_review_the_order(String arg1) throws Throwable {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".tu-c-checkout-order-summary-proceed-to-payment__form .tu-c-checkout-order-summary-proceed-to-payment__button")));
		driver.findElement(By.cssSelector(".tu-c-checkout-order-summary-proceed-to-payment__form .tu-c-checkout-order-summary-proceed-to-payment__button")).click();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/payment-method/add", driver.getCurrentUrl());

	}
	
	@When("^I choose \"([^\"]*)\", enter address, click on \"([^\"]*)\"$")
	public void i_choose_enter_address_click_on(String arg1, String arg2) throws Throwable {
		wait = new WebDriverWait(driver,10);
		driver.findElements(By.cssSelector(".ln-c-form-option__label")).get(1).click();
		driver.findElement(By.cssSelector(".ln-c-button.ln-c-button--primary")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("address.title")));
		Select titleDropdown = new Select(driver.findElement(By.id("address.title")));
		titleDropdown.selectByValue("mrs");
		driver.findElement(By.cssSelector("input[id='address.firstName']")).clear();
		driver.findElement(By.cssSelector("input[id='address.firstName']")).sendKeys("kkk");
		driver.findElement(By.cssSelector("input[id='address.surname']")).clear();
		driver.findElement(By.cssSelector("input[id='address.surname']")).sendKeys("sss");
		driver.findElement(By.cssSelector("input[id='addressPostcode']")).clear();
		driver.findElement(By.cssSelector("input[id='addressPostcode']")).sendKeys("rg14 2fb");
		driver.findElement(By.cssSelector(".ln-c-button--primary.address-lookup.ln-u-push-bottom")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#addressListView")));
		Select addressDropdown = new Select(driver.findElement(By.cssSelector("#addressListView")));
		addressDropdown.selectByIndex(17);
		driver.findElement(By.cssSelector("input[id='continue']")).click();
	}

	@When("^I choose \"([^\"]*)\", click on continue button, review order, \"([^\"]*)\"$")
	public void i_choose_click_on_continue_button_review_order(String arg1, String arg2) throws Throwable {
		wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("label[data-testid='deliveryMethod']"))).click();
		driver.findElement(By.cssSelector("input[data-testid='continue']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button[data-testid='submit-button']")));
		driver.findElement(By.cssSelector("button[data-testid='submit-button']")).click();
	}

	@Then("^I should see Payment options$")
	public void i_should_see_Payment_options() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/payment-method/add", driver.getCurrentUrl());
	}






}

