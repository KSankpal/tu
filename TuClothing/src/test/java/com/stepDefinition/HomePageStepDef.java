package com.stepDefinition;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomePageStepDef extends BaseClass {
	
	@Given("^I am in Home Page$")
	public void i_am_in_Home_Page() throws Throwable {
		homePage.verifyHomePage();
	}
	
	@When("^I search for Valid Product Name$")
	public void i_search_for_Valid_Product_Name() throws Throwable {
		homePage.searchWithValidProductName();
	    
	}
	
	@When("^I search for Invalid product name$")
	public void i_search_for_Invalid_product_name() throws Throwable {
		homePage.searchWithInvalidProductName();
	}

	@When("^I search for product with empty box$")
	public void i_search_for_product_with_empty_box() throws Throwable {
		homePage.searchWithEmptyBox();
	}
	
	@When("^I search for Valid product Category name$")
	public void i_search_for_Valid_product_Category_name() throws Throwable {
		homePage.searchWithValidProductCategoryName();
	}
	
	@When("^I search for colour$")
	public void i_search_for_colour() throws Throwable {
		homePage.searchWithColour();
	}

	@When("^I Move the cursor on the Search Icon$")
	public void i_Move_the_cursor_on_the_Search_Icon() throws Throwable {
		homePage.searchWithEmptyBox();
	}
	
	@Then("^the Search Icon should be Highlighted$")
	public void the_Search_Icon_should_be_Highlighted() throws Throwable {
		homePage.verifySearchIcon();
	}
	
	@When("^I Move the cursor on the Search Box$")
	public void i_Move_the_cursor_on_the_Search_Box() throws Throwable {
		homePage.searchTextBox();
	}
	
	@Then("^the cursor should be pointed to the left$")
	public void the_cursor_should_be_pointed_to_the_left() throws Throwable {
		homePage.verifySearchTextBox();
	}
	
	@When("^I Enter Valid product number$")
	public void i_Enter_Valid_product_number() throws Throwable {
		homePage.searchForValidProductNumber();
	}
	
	@When("^I Enter different brand name$")
	public void i_Enter_different_brand_name() throws Throwable {
		homePage.searchForDifferentBrands();
	}
	
	@When("^I Enter Invalid product number$")
	public void i_Enter_Invalid_product_number() throws Throwable {
		homePage.searchWithInvalidProductNumber();
	}
	
	@When("^I Enter Special characters$")
	public void i_Enter_Special_characters() throws Throwable {
		homePage.searchWithSpecialCharacters();
	}

	@When("^I Click on Tu Store locator$")
	public void i_Click_on_Tu_Store_locator() throws Throwable {
		homePage.tuStoreLocator();
	}
	
	


}
