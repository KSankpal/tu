Feature: End to End Scenarios

Scenario: Verify Place order with: Guest + Click & Collect
Given I am in Checkout Page  
When I Enter email in checkout as guest and click on "Guest Checkout" button 
And I choose "Click & Collect" Delivery Option,find nearest store and click on "Proceed to summary"
Then I review the order, "Proceed to Payment"

Scenario: Verify Place order with: Guest + Home Delivery
Given I am in Checkout Page  
When I Enter email in checkout as guest and click on "Guest Checkout" button 
And I choose "Home Delivery", enter address, click on "Continue"
And I choose "Standard Delivery", click on continue button, review order, "Proceed to payment"
Then I should see Payment options