Feature: Basket functionality

Scenario: Verify Add to basket functionality
Given I am in product detail page
And I choose the size/qty
When I Click on Add to basket
Then I see the product added to basket

Scenario: Verify Empty Basket Icon functionality 
Given I am Home Page
When I move the cursor on Basket image
Then I see a message Your basket is empty

Scenario: Verify Continue Shopping in Basket functionality
Given I am in Basket page with products added
When click on "continue shopping" button
Then I see Previous opened page

Scenario: Verify Remove button in Basket functionality
Given I am in Basket page with products added
When I click on remove button
Then I see the product removed

Scenario: Verify Add a promotion code/ pay with voucher in Basket functionality
Given I am in Basket page with products added
When I click on Add a promotion code/ pay with voucher 
Then I see an empty text box and apply button

Scenario: Verify Add a promotion code/ pay with voucher in Basket functionality with invalid code
Given I am in Basket page with products added
When I click on Add a promotion code/ pay with voucher 
And Enter Invalid code
Then I see an error message

