Feature: Sort by functionality

Scenario: Verify sort by functionality for Price high to Low
Given I am in Product
When I choose High to Low in Sort by
Then I see Products sorted 

Scenario: Verify sort by functionality for Price Low to High
Given I am in Product
When I choose Low to Highin Sort by
Then I see Products sorted


Scenario: Verify sort by functionality for Price Low to High
Given I am in Product
When I choose Relevance in Sort by
Then I see Products sorted

