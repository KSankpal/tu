Feature: Tu Login/Register functionality

Scenario: Verify Login/Register functionality
Given I am in Home Page
When I Click on Login/Register
Then I should see Login/Register page

Scenario: Verify Register icon functionality
Given I am in Home Page
When I Click on Login/Register
And Click on Register icon
Then I should see registration page

Scenario: Verify Register functionality with valid details
Given I am in Home Page
When I Click on Login/Register
And Click on Register icon
And Enter the valid relevant information 
And click on COMPLETE REGISTRATION 
Then I should see Thank you for registering message

Scenario: Verify Register functionality with Invalid email
Given I am in Home Page
When I Click on Login/Register
And Click on Register icon
And Enter the Invalid email 
And click on COMPLETE REGISTRATION 
Then I should see an error message to enter valid email

Scenario: Verify Register functionality with special character in Name field
Given I am in Home Page
When I Click on Login/Register
And Click on Register icon
And Enter special character 
And click on COMPLETE REGISTRATION 
Then I should see error message

Scenario: Verify Register functionality with empty text boxes
Given I am in Home Page
When I Click on Register icon 
And click on COMPLETE REGISTRATION 
Then I should see an error message to fill text box

Scenario: Verify Register functionality with less than 6-character password
Given I am in Home Page
When I Click on Register icon
And Enter less than 6-character password
And click on COMPLETE REGISTRATION 
Then I should see error message to re-enter password

Scenario: Verify Register functionality with Invalid nectar card number
Given I am in Home Page
When I Click on Register icon
And Enter Invalid nectar card number
And click on COMPLETE REGISTRATION 
Then I should see an error message as Invalid Nectar card number

Scenario: Verify Register functionality without ticking "I accept" check box
Given I am in Home Page
When I Click on Register icon
And click on COMPLETE REGISTRATION without ticking "I accept" check box
Then I should see an error message to click on I accept 

Scenario: Verify Register functionality without choosing "Title"
Given I am in Home Page
When I Click on Register icon
And click on COMPLETE REGISTRATION without choosing title
Then I should see an error message to choose title

Scenario: Verify Login functionality with valid Email ID and Password
Given I am in Home Page
When I Click on Login/Register 
And Enter valid Email ID and Password
Then I should see Tu home page

Scenario: Verify Login functionality with Invalid Email ID and valid Password
Given I am in Home Page
When I Click on Login/Register 
And Enter Invalid Email ID and valid Password
Then I should see error message.

Scenario: Verify Login functionality with valid Email ID and Invalid Password
Given I am in Home Page
When I Click on Login/Register 
And Enter valid Email ID and Invalid Password
Then I should see error message.

Scenario: Verify Login functionality with empty Email ID and valid Password
Given I am in Home Page
When I Click on Login/Register 
And Enter valid Password blank email
Then I should see error message.

Scenario: Verify Login functionality with valid Email ID and empty Password
Given I am in Home Page
When I Click on Login/Register 
And Enter valid email and blank Password 
Then I should see error message.









