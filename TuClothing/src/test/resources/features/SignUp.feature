Feature: Sign Up functionality

Scenario: Verify Sign up functionality for valid Email 
Given I am in Home Page
When I enter valid Email in Sign up to get marketing and offers from Tu 
Then I see a message "Thanks, that�s all done"

Scenario: Verify Sign up functionality for Invalid Email 
Given I am in Home Page
When I enter Invalid Email in Sign up to get marketing and offers from Tu 
Then I see a message "Thanks, that�s all done"

Scenario: Verify Sign up functionality for Blank Email 
Given I am in Home Page
When I enter Blank Email in Sign up to get marketing and offers from Tu 
Then I see a message "Thanks, that�s all done"