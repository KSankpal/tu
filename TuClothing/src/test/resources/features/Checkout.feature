Feature: Checkout functionality

Scenario: Verify Checkout functionality
Given I am in Home Page
And products added to basket
When I Click on Checkout 
Then I see Basket page

Scenario: Verify Proceed to checkout functionality
Given I am in Basket page
When I Click on Proceed to checkout 
Then I see relevant page

Scenario: Verify Click & collect functionality with basket under �20
Given I am in Basket page with products added in the basket
When I do a guest checkout 
Then I see click & collect option is unavailable

Scenario: Verify Click & collect functionality with basket above �20
Given I am in Basket page with basket above twenty pounds
When I do a guest checkout  
Then I see click & collect option available

Scenario: Verify Home Delivery functionality
Given I am in Basket page with products added to basket 
When I do a guest checkout   
Then I can opt Home Delivery 
