Feature: Search Functionality
Background:
Given I am in Home Page

Scenario: Verify Search Functionality for Valid Product Name
When I search for Valid Product Name
Then I should see the Search related products

Scenario: Verify Search functionality for Invalid product name
When I search for Invalid product name
Then I should see dissimilar products


Scenario: Verify Search functionality with empty box
When I search for product with empty box
Then I should see an error massage


Scenario: Verify Search functionality with Valid product Category name
When I search for Valid product Category name 
Then I should see the search related products


Scenario: Verify Search functionality for colour
When I search for colour
Then I should see the colour related products


Scenario: Verify Search functionality for Search Icon
When I Move the cursor on the Search Icon
Then the Search Icon should be Highlighted


Scenario: Verify Search functionality for Search Box
When I Move the cursor on the Search Box
Then the cursor should be pointed to the left


Scenario: Verify Search functionality for product number
When I Enter Valid product number
Then I should see item with matching product number


Scenario: Verify Search functionality for different brands
When I Enter different brand name 
Then I should the search related products


Scenario: Verify Search functionality for Invalid product number
When I Enter Invalid product number 
Then I should see an error massage Sorry no results found


Scenario: Verify Search functionality for Special characters
When I Enter Special characters
Then I should see an error message


