Feature: Follow Us

Scenario: Verify Follow Us functionality for Facebook 
Given I am in Home Page
When I Click on Facebook logo located on the footer
Then I see Tu Facebook homepage page

Scenario: Verify Follow Us functionality for Twitter
Given I am in Home Page
When I Click on Twitter logo located on the footer
Then I see Tu Twitter homepage page

Scenario: Verify Follow Us functionality for Instagram
Given I am in Home Page
When I Click on Instagram logo located on the footer
Then I see Tu Instagram homepage page

Scenario: Verify Follow Us functionality for Google Plus
Given I am in Home Page
When I Click on Google Plus logo located on the footer
Then I see Tu Google Plus homepage page

Scenario: Verify Follow Us functionality for Pinterest
Given I am in Home Page
When I Click on Pinterest logo located on the footer
Then I see Tu Pinterest homepage page
