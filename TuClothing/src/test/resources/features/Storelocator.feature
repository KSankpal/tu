Feature: Store Locator functionality
Background:
Given I am in Home Page
When I Click on Tu Store locator

Scenario: Verify Tu Store Locator functionality
Then I should see relevant page

Scenario: Verify Tu Store Locator functionality with valid postcode 
And I Enter valid postcode
Then I should see relevant page

Scenario: Verify Tu Store Locator functionality with Invalid postcode 
And I Enter Invalid postcode
Then I should see relevant page

Scenario: Verify Tu Store Locator functionality with valid town
And I Enter valid town
Then I should see relevant page

Scenario: Verify Tu Store Locator functionality with Invalid town
And I Enter Invalid town
Then I should see relevant page

Scenario: Verify Tu Store Locator functionality with valid postcode/town and selecting only Women check box
And I Enter valid town/postcode
And select women checkbox
Then I should see relevant page

Scenario: Verify Tu Store Locator functionality with valid postcode/town and selecting only men check box
And I Enter valid town/postcode
And select men checkbox
Then I should see relevant page 

Scenario: Verify Tu Store Locator functionality with valid postcode/town and selecting only children check box
And I Enter valid town/postcode
And select children checkbox
Then I should see relevant page
@test
Scenario: Verify Tu Store Locator functionality with valid postcode/town and selecting only click and collect check box
And I Enter valid town/postcode
And select click and collect check box 
Then I should see relevant page

Scenario: Verify Tu Store Locator functionality with empty Text box
And I Click on Find store
Then I should see relevant page

Scenario: Verify Tu Store Locator functionality with empty Text box and selecting click & collect
And Select click & collect check box 
And Click on Find store
Then I should see relevant page

Scenario: Verify Tu Store Locator functionality with valid postcode/town in the text box, selecting Womens, Mens, Childrens check box and click & collect check box
And Enter valid town/postcode
And Select Womens, Mens, Childrens and click & collect check box 
And Click on Find store
Then I should see relevant page


Scenario: Verify Tu Store Locator functionality with Invalid postcode/town in the text box, selecting Womens, Mens, Childrens check box and click & collect check box
And Enter Invalid town/postcode
And Select Womens, Mens, Childrens and click & collect check box 
And Click on Find store
Then I should see relevant page









